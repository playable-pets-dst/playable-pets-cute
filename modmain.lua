
return
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppcu_strings")

GLOBAL.PPCU_FORGE = require("ppcu_forge")

local TUNING = require("ppcu_tuning")

------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

----------------------------------------------------------------------
PrefabFiles = {
	
	--"cumonster_wpn",
	---homes---
	"rabbithome",
	"rabbithole_p",
	"molehome",
	"beehome",
	--"froghome",
	"molehole_p",
	"beehive_p"
	--"pond_p",
}

GLOBAL.PPCU_MobCharacters = {
	rabbitp        = { fancyname = "Rabbit",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true},
	pengullp        = { fancyname = "Pengull",           gender = "FEMALE",   mobtype = {}, skins = {}},
	beep        = { fancyname = "Bee",           gender = "MALE",   mobtype = {}, skins = {}},
	glommerp        = { fancyname = "Glommer",           gender = "FEMALE",   mobtype = {}, skins = {}},
	frogp        = { fancyname = "Frog",           gender = "MALE",   mobtype = {}, skins = {}},
	molep        = { fancyname = "Moleworm",           gender = "MALE",   mobtype = {}, skins = {}},
	perdp        = { fancyname = "Gobbler",           gender = "FEMALE",   mobtype = {}, skins = {}},
	flyp        = { fancyname = "Mosquito",           gender = "MALE",   mobtype = {}, skins = {}},
	gekkop        = { fancyname = "Grass Gekko",           gender = "MALE",   mobtype = {}, skins = {}},
	elephantplayer        = { fancyname = "Koalefant",           gender = "MALE",   mobtype = {}, skins = {}},
	chesterp        = { fancyname = "Chester",           gender = "MALE",   mobtype = {}, skins = {}},
	crowp        = { fancyname = "Crow",           gender = "FEMALE",   mobtype = {}, skins = {}},
	birdp        = { fancyname = "Robin",           gender = "MALE",   mobtype = {}, skins = {}},
	buzzardp        = { fancyname = "Buzzard",           gender = "MALE",   mobtype = {}, skins = {}},
	mandrakep        = { fancyname = "Mandrake",           gender = "MALE",   mobtype = {}, skins = {}},
	birchnutterp        = { fancyname = "Birchnutter",           gender = "MALE",   mobtype = {}, skins = {}},
	butterflyp        = { fancyname = "Rabbit",           gender = "FEMALE",   mobtype = {}, skins = {}},
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPCU_Character_Order = {
	"rabbitp",
	"butterflyp",
	"crowp",
	"birdp",
	"beep",
	"glommerp",
	"frogp",
	"molep",
	"perdp",
	"flyp",
	"mandrakep",
	"birchnutterp",
	"gekkop",
	"chesterp",
	"buzzardp",
	"pengullp",
	"elephantplayer",
}

Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"),
}

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS
---------------------------------------------------------
--Custom Recipes
local Ingredient = GLOBAL.Ingredient
--local pigrecipe = AddRecipe("pighouse_player", {Ingredient("boards", 4),Ingredient("cutstone", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pighouse_placer", nil, nil, 1, "pigmanplayer", "images/inventoryimages/houseplayer.xml", "houseplayer.tex")
local MobHouseMethod = PlayablePets.GetModConfigData("MobHouseMethod")

-- Add home crafting recipes if enabled
if MobHouseMethod == SETTING.HOUSE_ON_CRAFT or MobHouseMethod == SETTING.HOUSE_BOTH then
	local rabbitholerecipe = AddRecipe("rabbithole_p", {Ingredient("shovel", 1),Ingredient("carrot", 2), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "rabbithole_p_placer", nil, nil, 1, nil, "images/inventoryimages/rabbithome.xml", "rabbithome.tex")
	local molerecipe = AddRecipe("molehole_p", {Ingredient("shovel", 1),Ingredient("flint", 2), Ingredient("rocks", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "molehole_p_placer", nil, nil, 1, nil, "images/inventoryimages/molehome.xml", "molehome.tex")
	local beerecipe = AddRecipe("beehive_p", {Ingredient("honeycomb", 1),Ingredient("honey", 3), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "beehive_p_placer", nil, nil, 1, nil, "images/inventoryimages/beehome.xml", "beehome.tex")
	--local bee2recipe = AddRecipe("beehive2_p", {Ingredient("honeycomb", 2),Ingredient("stinger", 3), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "beehive2_p_placer", nil, nil, 1, nil, "images/inventoryimages/beehome.xml", "beehome.tex")
	--local frogrecipe = AddRecipe("pond_p", {Ingredient("shovel", 1),Ingredient("reeds", 6), Ingredient("froglegs", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pond_p_placer", nil, nil, 1, nil, "images/inventoryimages/froghome.xml", "froghome.tex")
end
---------------------------------------------------------
--PostInits
------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------
local function FriendlyWaspInit(prefab)
	local function OnNearBy(prefab, target)
	if prefab.components.childspawner ~= nil and not target:HasTag("bee") then
        prefab.components.childspawner:ReleaseAllChildren(target, "killerbee")
    end

	end
	
	if prefab.components.playerprox then
	prefab.components.playerprox:SetOnPlayerNear(OnNearBy)
	end
end

AddPrefabPostInit("wasphive", FriendlyWaspInit)--function(inst)

-------------------------------------------------------
--Wardrobe stuff--
local MobPuppets = require("ppcu_puppets")
local MobSkins = require("ppcu_skins")
PlayablePets.RegisterPuppetsAndSkins(PPCU_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPCU_Character_Order) do
	local mob = GLOBAL.PPCU_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPCU_Character_Order) do
	local mob = GLOBAL.PPCU_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab)
	
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
	end
end
