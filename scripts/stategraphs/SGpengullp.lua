require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat_pre"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("idle") end end),
    EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
    PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnSink(),
	CommonHandlers.OnHop(), 
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OpenGift(),
}

local states=
{
    
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:KillSound("slide")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

        

    },    
    
   State{  name = "idle",
            tags = {"idle", "canrotate"},
            onenter = function(inst, playanim)
                inst.Physics:Stop()
                inst.components.locomotor:Stop()
                inst.SoundEmitter:KillSound("slide")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/idle")
                inst.AnimState:PlayAnimation("idle_loop", true)
            end,
            
            timeline = 
            {
                --TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/bishop/idle") end ),
            },
            
            events=
            {
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
            },
        },

    State{  name = "run_start",
            tags = {"moving", "running", "canrotate"},
            
            onenter = function(inst)
                inst.components.locomotor:RunForward()
                inst.AnimState:SetTime(math.random()*2)
                inst.SoundEmitter:KillSound("slide")
                if TheWorld.state.snowlevel < 0.1 then
                    inst.SoundEmitter:PlaySound(inst._soundpath.."land")
                else
                    inst.SoundEmitter:PlaySound(inst._soundpath.."land_dirt")
                end
                inst.AnimState:PlayAnimation("slide_loop")
            end,

            events =
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
            },
            
            onexit = function(inst)
                if TheWorld.state.iswinter then
                    inst.SoundEmitter:PlaySound(inst._soundpath.."slide","slide")
                else
                    inst.SoundEmitter:PlaySound(inst._soundpath.."slide_dirt","slide")
                end
            end,

            timeline=
            {
            },        
        },
		
	State{  name = "run",
            tags = {"moving", "running", "canrotate"},
            
            onenter = function(inst) 
                inst.components.locomotor:RunForward()
                inst.AnimState:PlayAnimation("slide_loop", true)
            end,
            
            timeline=
            {
            },
            
            events =
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
            },
        },
    
    State{  name = "run_stop",
            tags = {"canrotate", "idle"},
            
            onenter = function(inst) 
                inst.SoundEmitter:KillSound("slide")
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("slide_post")
            end,
            
            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
            },
        },    

    
    State{  name = "walk_start",
            tags = {"moving", "canrotate"},
            
            onenter = function(inst)
                inst.SoundEmitter:KillSound("slide")
                inst.components.locomotor:WalkForward()
                -- inst.AnimState:SetTime(math.random()*2)
                inst.AnimState:PlayAnimation("walk")
            end,

            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("walk") end ),        
            },
        },      
    
    State{  name = "walk",
            tags = {"moving", "canrotate"},
            
            onenter = function(inst) 
                inst.components.locomotor:WalkForward()
                inst.AnimState:PlayAnimation("walk", true)
                inst.SoundEmitter:KillSound("slide")
                inst.SoundEmitter:PlaySound(inst._soundpath.."idle")
            end,
    
            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("walk") end ),        
            },

            timeline = {
                TimeEvent(5*FRAMES, function(inst)
                                        if TheWorld.state.iswinter then
                                            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/footstep")
                                        else
                                            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/footstep_dirt")
                                        end
                                    end),
                TimeEvent(21*FRAMES, function(inst)
                                        if TheWorld.state.iswinter then
                                            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/footstep")
                                        else
                                            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/footstep_dirt")
                                        end
                                    end),
            },
        },

    State{  name = "walk_stop",
            tags = {"canrotate", "idle"},
            
            onenter = function(inst) 
                inst.SoundEmitter:KillSound("slide")
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("idle_loop", true)
            end,

            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
            },
        },   
    
    State{  name = "eat_pre",
            tags = {"busy"},
            onenter = function(inst)
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("atk_pre", false)
                inst.SoundEmitter:KillSound("slide")
            end,

            timeline = 
            {
                TimeEvent(4*FRAMES, function(inst) 
                                        inst:PerformBufferedAction()
                                        --inst.SoundEmitter:PlaySound("dontstarve/creatures/slurtle/bite")
                                     end ), --take food
            },        
            
            events = 
            {
                EventHandler("animover", function(inst) inst.sg:GoToState("eat_loop") end)
            },

        },


    State{  name = "eat_loop",
            tags = {"busy"},
            onenter = function(inst)
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("eat", true)
                inst.sg:SetTimeout(0.8+math.random())
            end,

            timeline = 
            {
            },

            events = 
            {
            },
            
            ontimeout= function(inst)
                inst.lastmeal = GetTime()
                inst:PerformBufferedAction()
                inst.sg:GoToState("idle", "walk")
            end,
        }, 

    State{  name = "pickup",
            tags = {"busy"},
            onenter = function(inst)
                inst.SoundEmitter:KillSound("slide")
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("atk_pre")
                inst.AnimState:PushAnimation("atk", true)
                inst.sg:SetTimeout(0.2)
            end,

            timeline = 
            {
            },

            events = 
            {
                EventHandler("attacked",
                             function(inst)
                                 inst.components.inventory:DropItem(inst:GetBufferedAction().target)
                                 inst.sg:GoToState("idle")
                             end) --drop food
            },
            
            ontimeout= function(inst)
                            inst.lastmeal = GetTime()
                            inst:PerformBufferedAction()
                            inst.sg:GoToState("idle")
                        end,
        }, 
		
	 State{  name = "jumpin",
            tags = {"busy"},
            onenter = function(inst)
                inst.SoundEmitter:KillSound("slide")
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("atk_pre")
                inst.AnimState:PushAnimation("atk", true)
                inst.sg:SetTimeout(0.2)
            end,

            timeline = 
            {
            },

            events = 
            {
                EventHandler("attacked",
                             function(inst)
                                 inst.components.inventory:DropItem(inst:GetBufferedAction().target)
                                 inst.sg:GoToState("idle")
                             end) --drop food
            },
            
            ontimeout= function(inst)
                            inst.lastmeal = GetTime()
                            inst:PerformBufferedAction()
                            inst.sg:GoToState("idle")
                        end,
        }, 	

    State{  name = "action",
            onenter = function(inst, playanim)
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("slide_post")
                inst:PerformBufferedAction()
            end,
            timeline = 
            {
                TimeEvent(GetRandomWithVariance(30,15)*FRAMES, function(inst)
                                        inst.sg:GoToState("walk_start") 
                                     end),
            },
            --[[
            events=
            {
                EventHandler("animover", function (inst)
                    inst.sg:GoToState("idle")
                end),
            }
            --]]
        },  

    State{  name = "migrate",
            onenter = function(inst, playanim)
                inst.SoundEmitter:KillSound("slide")
                inst:PerformBufferedAction()
                inst.components.locomotor:WalkForward()
                inst.AnimState:PlayAnimation("walk", true)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/idle")
            end,
            timeline = {
                TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/footstep") end),
                TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/footstep") end),
            },
            events=
            {
                EventHandler("animover", function (inst)
                    inst.sg:GoToState("walk_start")
                end),
            }
        },  
		
		State{  name = "appear",
            tags = {"busy"},
            
            onenter = function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/splash")
                inst.Physics:Stop()	
                inst.AnimState:PlayAnimation("slide_pre")
            end,

            events =
            {
                EventHandler("animqueueover", function(inst) inst.sg:GoToState("landing") end),
            },
        },
        
    State{  name = "landing",
            tags = {"busy"},
            
            onenter = function(inst)
                inst.components.locomotor:RunForward()
                inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/jumpin")
                inst.AnimState:PushAnimation("slide_loop", "loop")
            end,
            
            timeline = 
            {
                TimeEvent(GetRandomWithVariance(30,15)*FRAMES, function(inst)
                                        inst.sg:GoToState("walk_start") 
                                        --inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/slide")
                                     end),
            },
        },
        
   State{ name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
			inst:PerformBufferedAction()
            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/taunt")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{ name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
			inst:PerformBufferedAction()
            inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/taunt")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{ name = "special_atk2",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("slide_post")
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end			
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{  name = "attack",
            tags = {"attack"},
            
            onenter = function(inst,target)
                --eprint(inst,"StateAttack onenter:",target,inst.sg.statemem.target)
                if target then
                    inst.sg.statemem.target = target
                end
                inst.SoundEmitter:KillSound("slide")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/attack")
                inst.components.combat:StartAttack()
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("atk_pre")
                inst.AnimState:PushAnimation("atk", false)
            end,
            
            timeline =
            {
                TimeEvent(15*FRAMES, function(inst)
                    inst:PerformBufferedAction()
                end),
            },
            
            events =
            {
                EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
            },
        },

    State{ name = "flyaway",
        tags = {"flight", "busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.Physics:ClearCollisionMask()
            inst.Physics:SetCollides(false)
            inst.sg:SetTimeout(.1+math.random()*.2)
            inst.sg.statemem.vert = math.random() > .1

            inst.Physics:SetMotorVelOverride(00,15,0)
            
	        inst.DynamicShadow:Enable(false)
            -- inst.SoundEmitter:PlaySound(inst.sounds.takeoff)
            
            if inst.sg.statemem.vert then
                --inst.AnimState:PlayAnimation("takeoff_vertical_pre")
                inst.AnimState:PushAnimation("atk_pre", true)
            else
                --inst.AnimState:PlayAnimation("takeoff_diagonal_pre")
                inst.AnimState:PushAnimation("atk_pre", true)
            end
        end,
        
        ontimeout= function(inst)
            if inst.sg.statemem.vert then
                --inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
                inst.AnimState:PushAnimation("idle_loop", true)
                --inst.Physics:SetMotorVel(-1 + math.random(),15+(math.random()*5),-2 + math.random())
                inst.Physics:SetMotorVelOverride(00,15,0)
            else
                --inst.AnimState:PushAnimation("takeoff_diagonal_loop", true)
                inst.AnimState:PushAnimation("idle_loop", true)
                local x = 8+ math.random()*8
                --inst.Physics:SetMotorVel(x,15+(math.random()*5),-2 + math.random()*4)
                inst.Physics:SetMotorVelOverride(00,15,0)
            end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("flyaway") end),
        },
        
        timeline = 
        {
            TimeEvent(2, function(inst) inst:Remove() end)
            --TimeEvent(8*FRAMES, function(inst) inst.Physics:SetMotorVelOverride(20,0,0) end),
        }
        
    },

    State{  name = "runningattack",
            tags = {"runningattack"},
            
            onenter = function(inst)
                inst.SoundEmitter:KillSound("slide")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/attack")
                inst.components.combat:StartAttack()
                --inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("slide_bounce")
            end,
            
            timeline =
            {
                TimeEvent(1*FRAMES, function(inst)
                                        inst.components.combat:DoAttack()
                                     end),
            },
            
            events =
            {
                EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk_start") end),
            },
        },


    
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/fallasleep")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = 
		{
            TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{

		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"slide_post", nil, nil, "taunt", "slide_post") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst._soundpath.."death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst._soundpath.."taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "slide_post")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddSimpleState(states,"hit","hit", {"busy"})
CommonStates.AddHopStates(states, false, {pre = "slide_bounce", loop = "slide_loop", pst = "slide_post"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle_loop",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "idle_loop",
	
	plank_hop_pre = "slide_bounce",
	plank_hop = "slide_loop",
	
	steer_pre = "idle_loop",
	steer_idle = "idle_loop",
	steer_turning = "taunt",
	stop_steering = "idle_loop",
	
	row = "slide_post",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "slide_loop",
	
	leap_pre = "slide_bounce",
	leap_loop = "slide_loop",
	leap_pst = "slide_post",
	
	castspelltime = 10,
})


    
return StateGraph("pengullp", states, events, "idle", actionhandlers)

