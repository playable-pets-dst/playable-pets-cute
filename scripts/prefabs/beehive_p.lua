require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/spider_cocoon.zip"),
    Asset("SOUND", "sound/spider.fsb"),
	Asset("MINIMAP_IMAGE", "spiderden"), --shared with spiderden2 and 3
}

local prefabs =
{
    --"merm",
    "collapse_big",

    --loot:
    --"logs",
    --"rocks",
    --"fish",
}

local loot =
{
	"honey",
	"honey",
	"honeycomb",

}

local function onhammered(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    inst:RemoveComponent("childspawner")
	inst.AnimState:PlayAnimation("cocoon_dead", true)
    RemovePhysicsColliders(inst)

    inst.SoundEmitter:KillSound("loop")

    inst.SoundEmitter:PlaySound("dontstarve/bee/beehive_destroy")
    inst.components.lootdropper:DropLoot()
   
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        if inst.components.childspawner ~= nil then
            inst.components.childspawner:ReleaseAllChildren(worker)
        end
        inst.AnimState:PlayAnimation("cocoon_small_hit")
        inst.AnimState:PushAnimation("coocoon_small", true)
    end
end

local function onbuilt(inst)
    --inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("cocoon_small", true)
end



local function OnEntityWake(inst)
    --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/mound_LP", "loop")
end

local function OnEntitySleep(inst)
    --inst.SoundEmitter:KillSound("loop")
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function onignite(inst)
    inst.components.sleepingbag:DoWakeUp()
end

local function onburntup(inst)
    --inst.AnimState:PlayAnimation("burnt_rundown")
end

local function wakeuptest(inst, phase)
    --if phase ~= inst.sleep_phase then
        --inst.components.sleepingbag:DoWakeUp()
    --end
end

local function onwake(inst, sleeper, nostatechange)
    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
        inst.sleeptask = nil
    end
	inst.SoundEmitter:KillSound("loop")
	
	--inst.AnimState:PlayAnimation("hit")
	
    inst:StopWatchingWorldState("phase", wakeuptest)
    sleeper:RemoveEventCallback("onignite", onignite, inst)

    if not nostatechange then
        if sleeper.sg:HasStateTag("house_sleep") then
            sleeper.sg.statemem.iswaking = true
        end
		--inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
        sleeper.sg:GoToState("taunt")
    end

    if inst.sleep_anim ~= nil then
        inst.AnimState:PushAnimation("cocoon_small", true)
    end

    --inst.components.finiteuses:Use()
end

local function OnKilled(inst)
    inst.AnimState:PlayAnimation("death", false)
    inst.SoundEmitter:KillSound("loop")
    inst.components.multisleepingbag:DoWakeUpAll()
    inst.components.lootdropper:DropLoot(inst:GetPosition())
end

local function onwake(inst, sleeper, nostatechange)
    if not nostatechange then
        if sleeper.sg:HasStateTag("house_sleep") then
            sleeper.sg.statemem.iswaking = true
        end
    end

    if inst.sleep_anim ~= nil then
        --inst.AnimState:PushAnimation("idle", true)
    end

    if #inst.components.multisleepingbag:GetSleepers() <= 0 then
        inst.SoundEmitter:KillSound("loop")
    end
end

local function onsleeptick(inst, sleepers)
	if sleepers and #sleepers > 0 then
		for i, sleeper in ipairs(sleepers) do
			local isstarving = sleeper.components.beaverness ~= nil and sleeper.components.beaverness:IsStarving()
	
			if not sleeper:HasTag(inst.components.multisleepingbag.musthavetag) then
				inst.components.multisleepingbag:DoWakeUp(sleeper)
			end
	
			if sleeper.components.hunger ~= nil then
				sleeper.components.hunger:DoDelta(inst.hunger_tick, true, true)
				isstarving = sleeper.components.hunger:IsStarving()
			end

			if sleeper.components.sanity ~= nil and sleeper.components.sanity:GetPercentWithPenalty() < 1 then
				sleeper.components.sanity:DoDelta(TUNING.SLEEP_SANITY_PER_TICK * 2, true)
			end

			if not isstarving and sleeper.components.health ~= nil then
				sleeper.components.health:DoDelta(TUNING.SLEEP_HEALTH_PER_TICK, true, inst.prefab, true)
			end

			if sleeper.components.temperature ~= nil then
				if inst.is_cooling then
					if sleeper.components.temperature:GetCurrent() > TUNING.SLEEP_TARGET_TEMP_TENT then
						sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() - TUNING.SLEEP_TEMP_PER_TICK)
					end
				elseif sleeper.components.temperature:GetCurrent() < TUNING.SLEEP_TARGET_TEMP_TENT then
					sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() + TUNING.SLEEP_TEMP_PER_TICK)
				end
			end

			if isstarving then
				inst.components.multisleepingbag:DoWakeUp(sleeper)
			end
		end	
	end
end

local function onsleep(inst, sleeper)
    if #inst.components.multisleepingbag:GetSleepers() > 0 and not inst.SoundEmitter:PlayingSound("loop") then
	    inst.SoundEmitter:PlaySound("dontstarve/bee/bee_hive_LP", "loop")
    end
end

local fn = function()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    --MakeObstaclePhysics(inst, 0.5)

	
	inst.hunger_tick = 0
	
	MakeSnowCoveredPristine(inst)
	
	MakeSmallObstaclePhysics(inst, .5)

    inst.MiniMapEntity:SetIcon("beehive.png")

    inst.AnimState:SetBank("beehive")
    inst.AnimState:SetBuild("beehive")
    inst.AnimState:PlayAnimation("cocoon_small", true)

    inst:AddTag("structure")
	inst:AddTag("beehouse")
    inst:AddTag("mobhome")

	inst.mobhouse = true

    inst:SetPrefabNameOverride("beehive")
	
	inst:AddComponent("heater") 
	inst.components.heater.heat = 40
		
    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	-------------------
    --inst:AddComponent("health")
    --inst.components.health:SetMaxHealth(300)
    --inst:ListenForEvent("death", OnKilled)

    -------------------
	
	inst:AddComponent("workable")
	inst:AddComponent("multisleepingbag")
	inst.components.multisleepingbag:MustHaveTag("bee")
	inst.components.multisleepingbag.onsleep = onsleep
    inst.components.multisleepingbag.onwake = onwake
    --convert wetness delta to drying rate
    inst.components.multisleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)
	inst.hunger_tick = TUNING.SLEEP_HUNGER_PER_TICK / 3
	inst.sleeptask = inst:DoPeriodicTask(TUNING.SLEEP_TICK_PERIOD, onsleeptick, nil, inst.components.multisleepingbag.sleepers)

	
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(3)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
	
    inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot(loot)

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_SMALL)
	
	inst.data = {}

    MakeMediumPropagator(inst)


    inst:AddComponent("inspectable")

    MakeSnowCovered(inst)
	
	inst.OnEntitySleep = OnEntitySleep
    inst.OnEntityWake = OnEntityWake

    return inst
end

return Prefab("beehive_p", fn, assets, prefabs),
	MakePlacer("beehive_p_placer", "beehive", "beehive", "cocoon_small")
