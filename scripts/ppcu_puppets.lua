local DEFAULT_SCALE = 0.20
local Puppets = {
	--Cute
    rabbitp = {bank = "rabbit", build = "rabbit_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "rabbit" },
	butterflyp = {bank = "butterfly", build = "butterfly_basic", anim = "idle_flight_loop", scale = DEFAULT_SCALE, shinybuild = "rabbit" },
	beep = {bank = "bee", build = "bee_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "bee" },
	birchnutterp = {bank = "treedrake", build = "treedrake_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "birchnutter" },
	birdp = {bank = "crow", build = "robin_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "robin" },
	crowp = {bank = "crow", build = "crow_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "robin" },
	buzzardp = {bank = "buzzard", build = "buzzard_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "buzzard" },
	chesterp = {bank = "chester", build = "chester_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "chester" },
	elephantplayer = {bank = "koalefant", build = "koalefant_summer_build", anim = "idle_loop", scale = DEFAULT_SCALE - 0.05, shinybuild = "koalefant_summer"},
	flyp = {bank = "mosquito", build = "mosquito", anim = "walk_loop", scale = DEFAULT_SCALE, shinybuild = "mosquito" },
	chesterp = {bank = "chester", build = "chester_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "chester" },
	frogp = {bank = "frog", build = "frog", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "frog" },
	gekkop = {bank = "grassgecko", build = "grassgecko", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "gressgecko" },
	glommerp = {bank = "glommer", build = "glommer", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "glommer", y_offset = -25},
	mandrakep = {bank = "mandrake", build = "mandrake", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "mandrake" },
	molep = {bank = "mole", build = "mole_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "mole" },
	pengullp = {bank = "penguin", build = "penguin_build", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "penguin" },
	perdp = {bank = "perd", build = "perd", anim = "idle_loop", scale = DEFAULT_SCALE, shinybuild = "perd", hide = {"HAT"}},
}

return Puppets