local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	elephantplayer = {
		cat = {
			name = "cat",
			fname = "Cat Koalefant",
			build = "koalefant_cat",
			build2 = "koalefant_cat",
		},
		dino = {
			name = "dino",
			fname = "Dinosaur Koalefant",
			build = "koalefant_dinosaur",
		},
		goldarmor = {
			name = "goldarmor",
			fname = "Gold Hauberk Koalefant",
			build = "koalefant_goldhauberk",
		},
		armor = {
			name = "armor",
			fname = "Hauberk Koalefant",
			build = "koalefant_hauberk",
		},
		moth = {
			name = "moth",
			fname = "Moth Koalefant",
			build = "koalefant_moth",
		},
		tapir = {
			name = "tapir",
			fname = "Tapir Koalefant",
			build = "koalefant_tapir",
		},
	},
	frogp = {
		odd = {
			name = "odd",
			fname = "Odd Frog",
			build = "frog_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Frog",
			build = "frog",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	glommerp = {
		odd = {
			name = "odd",
			fname = "Odd Glommer",
			build = "glommer",
			hue = 0.3, 
		},
		yellow = {
			name = "yellow",
			fname = "Yellow Glommer",
			build = "glommer",
			hue = 0.5, 
		},
		green = {
			name = "green",
			fname = "Green Glommer",
			build = "glommer",
			hue = 0.8, 
		},
		rgb = {
			name = "rgb",
			fname = "RGB Frog",
			build = "glommer",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	rabbitp = {
		odd = {
			name = "odd",
			fname = "Odd Rabbit",
			build = "rabbit_shiny_build_01",
		},
		rf = {
			name = "rf",
			fname = "Reforged Rabbit",
			build = "rabbit_shiny_build_08",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Rabbit",
			build = "rabbit_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
}

return SKINS