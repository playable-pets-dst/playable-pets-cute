
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES.butterflyp = "Butterfly"
STRINGS.CHARACTER_NAMES.butterflyp = "Butterfly"
STRINGS.CHARACTER_DESCRIPTIONS.butterflyp = "*Is a butterfly.\n*Is flying.\n*Can't attack despite it trying to."
STRINGS.CHARACTER_QUOTES.butterflyp = "After 7 years in development, hopefully it will be worth the wait."

STRINGS.CHARACTER_TITLES.rabbitp = "Rabbit"
STRINGS.CHARACTER_NAMES.rabbitp = "RABBIT"
STRINGS.CHARACTER_DESCRIPTIONS.rabbitp = "*Is cute.\n*Grows white fur in the winter..\n*Can't attack."
STRINGS.CHARACTER_QUOTES.rabbitp = "AAAAAAAAAAAA"

STRINGS.CHARACTER_TITLES.pengullp = "Pengull"
STRINGS.CHARACTER_NAMES.pengullp = "PENGULL"
STRINGS.CHARACTER_DESCRIPTIONS.pengullp = "*Is loud.\n*Lays eggs.\n*Loves winter."
STRINGS.CHARACTER_QUOTES.pengullp = "SQUAWK"

STRINGS.CHARACTER_TITLES.glommerp = "Glommer"
STRINGS.CHARACTER_NAMES.glommerp = "GLOMMER"
STRINGS.CHARACTER_DESCRIPTIONS.glommerp = "*Is cute.\n*Poops special poop.\n*Can't Attack..."
STRINGS.CHARACTER_QUOTES.glommerp = "ZZZRP"

STRINGS.CHARACTER_TITLES.frogp = "Frog"
STRINGS.CHARACTER_NAMES.frogp = "FROG"
STRINGS.CHARACTER_DESCRIPTIONS.frogp = "*Is a Frog.\n*Makes enemies drop items.\n*Is hated"
STRINGS.CHARACTER_QUOTES.frogp = "ribbit"

STRINGS.CHARACTER_TITLES.molep = "Mole"
STRINGS.CHARACTER_NAMES.molep = "MOLE"
STRINGS.CHARACTER_DESCRIPTIONS.molep = "*Hides underground.\n*Loves rocks.\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.molep = "..."

STRINGS.CHARACTER_TITLES.beep = "Bee"
STRINGS.CHARACTER_NAMES.beep = "BEE"
STRINGS.CHARACTER_DESCRIPTIONS.beep = "*Is a bee.\n*buzzes around.\n*Has a nasty sting."
STRINGS.CHARACTER_QUOTES.beep = "BZZZZZZ"

STRINGS.CHARACTER_TITLES.flyp = "Mosquito"
STRINGS.CHARACTER_NAMES.flyp = "MOSTQUITO"
STRINGS.CHARACTER_DESCRIPTIONS.flyp = "*Is a bloodsucker.\n*Is superfast.\n* Annoying..."
STRINGS.CHARACTER_QUOTES.flyp = "BZZZZZZZZZZ"

STRINGS.CHARACTER_TITLES.gekkop = "Grass Gekko"
STRINGS.CHARACTER_NAMES.gekkop = "GRASS GEKKO"
STRINGS.CHARACTER_DESCRIPTIONS.gekkop = "*Sheds grass.\n*Is superfast.\n*Is frail..."
STRINGS.CHARACTER_QUOTES.gekkop = "BZZZZZZZZZZ"

STRINGS.CHARACTER_TITLES.elephantplayer = "Koalefant"
STRINGS.CHARACTER_NAMES.elephantplayer = "KOALEFANT"
STRINGS.CHARACTER_DESCRIPTIONS.elephantplayer = "*Is Bulky.\n*Nice and Warm.\n*Is Adorable!"
STRINGS.CHARACTER_QUOTES.elephantplayer = "HUH?"

STRINGS.CHARACTER_TITLES.perdp = "Gobbler"
STRINGS.CHARACTER_NAMES.perdp = "GOBBLER"
STRINGS.CHARACTER_DESCRIPTIONS.perdp = "*Is fast.\n*Loves Berries.\n*ONLY loves Berries."
STRINGS.CHARACTER_QUOTES.perdp = "gobblegobble"

STRINGS.CHARACTER_TITLES.chesterp = "Chester"
STRINGS.CHARACTER_NAMES.chesterp = "CHESTER"
STRINGS.CHARACTER_DESCRIPTIONS.chesterp = "*Is Chester!\n*Is Helpful.\n*Can Transform."
STRINGS.CHARACTER_QUOTES.chesterp = "*pant*"

STRINGS.CHARACTER_TITLES.mandrakep = "Mandrake"
STRINGS.CHARACTER_NAMES.mandrakep = "MANDRAKE"
STRINGS.CHARACTER_DESCRIPTIONS.mandrakep = "*Is annoying!\n*It won't stop.\n*AAAAAAAAAAAAAAA"
STRINGS.CHARACTER_QUOTES.mandrakep = "MEEPMEEPMEEPMEEP"

STRINGS.CHARACTER_TITLES.crowp = "Crow"
STRINGS.CHARACTER_NAMES.crowp = "CROW"
STRINGS.CHARACTER_DESCRIPTIONS.crowp = "*Is adorable!\n*Can fly.\n*Is pretty frail"
STRINGS.CHARACTER_QUOTES.crowp = "CAW CAW"

STRINGS.CHARACTER_TITLES.birdp = "Robin"
STRINGS.CHARACTER_NAMES.birdp = "ROBIN"
STRINGS.CHARACTER_DESCRIPTIONS.birdp = "*Is adorable!\n*Can fly.\n*Is pretty frail"
STRINGS.CHARACTER_QUOTES.birdp = "CAW CAW"

STRINGS.CHARACTER_TITLES.buzzardp = "Buzzard"
STRINGS.CHARACTER_NAMES.buzzardp = "BUZZARD"
STRINGS.CHARACTER_DESCRIPTIONS.buzzardp = "*Is NOT adorable!\n*Can fly.\n*Can attack."
STRINGS.CHARACTER_QUOTES.buzzardp = "SQUAWK"

STRINGS.CHARACTER_TITLES.birchnutterp = "Birchnutter"
STRINGS.CHARACTER_NAMES.birchnutterp = "BIRCHNUTTER"
STRINGS.CHARACTER_DESCRIPTIONS.birchnutterp = "*Is NOT adorable!\n*Is annoying.\n*Hates fire."
STRINGS.CHARACTER_QUOTES.birchnutterp = "BAP"
----------------------------------------------------------------
-- Custom speech strings
--STRINGS.CHARACTERS.STALKERP = require "speech_stalkerp"
----------------------------------------------------------------
-- The default responses of examining the prefab

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOME = 
{
	GENERIC = "A rabbit must've dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOLE_P = 
{
	GENERIC = "There must be a rabbit around here.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MOLEHOME = 
{
	GENERIC = "A mole probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MOLEHOLE_P = 
{
	GENERIC = "I better keep an eye on my rocks.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHOME = 
{
	GENERIC = "A bee probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHIVE_P = 
{
	GENERIC = "That one seems pretty quiet.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHIVE2_P = 
{
	GENERIC = "Looks as angry as its owner.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.FROGHOME = 
{
	GENERIC = "A frog probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.POND_P = 
{
	GENERIC = "Theres no fish in there.",

}

------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
STRINGS.NAMES.RABBITP = "Rabbit"
STRINGS.NAMES.PENGULLP = "Pengull"
STRINGS.NAMES.GLOMMERP = "Glommer"
STRINGS.NAMES.FROGP = "Frog"
STRINGS.NAMES.MOLEP = "Mole"
STRINGS.NAMES.BEEP = "Bee"
STRINGS.NAMES.CHESTERP = "Chester"
STRINGS.NAMES.MANDRAKEP = "Mandrake"
----------------------------
STRINGS.NAMES.RABBITHOME = "Rabbit Home"
STRINGS.NAMES.RABBITHOLE_P = "Rabbit Hole"

STRINGS.NAMES.MOLEHOME = "Mole Home"
STRINGS.NAMES.MOLEHOLE_P = "Moleworm Hole"

STRINGS.NAMES.BEEHOME = "Bee Home"
STRINGS.NAMES.BEEHIVE_P = "Bee Hive"
STRINGS.NAMES.BEEHIVE2_P = "Killer Bee Hive"

STRINGS.NAMES.FROGHOME = "Frog Home"
STRINGS.NAMES.POND_P = "Pond"
------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.rabbitp = "*Is very weak\n*Less aggro\n*Can be revived 3x faster\n\nExpertise:\nMelee"
------------------------------------------------------------------
-- Mob strings
------------------------------------------------------------------


------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS