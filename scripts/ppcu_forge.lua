
local GIANT_HP_REDUCTION = 4

return	{
	
	RABBITP = {
		NAME = "rabbitp",
		HEALTH = 50,
	
		DAMAGE = 3,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 2,
	
		RUNSPEED = 7,
		WALKSPEED = 3,
		
		SPECIAL_CD = 10,
		
		DIFFICULTY = 3,	
		ITEMS = {"forge_woodarmor"}
	},
}