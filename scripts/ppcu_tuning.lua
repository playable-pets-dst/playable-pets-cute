for i, v in pairs(PPCU_FORGE) do
	TUNING.GAMEMODE_STARTING_ITEMS.LAVAARENA[string.upper(v.NAME)] = v.ITEMS or nil
	TUNING.LAVAARENA_SURVIVOR_DIFFICULTY[string.upper(v.NAME)] = v.DIFFICULTY or 1
	TUNING.LAVAARENA_STARTING_HEALTH[string.upper(v.NAME)] = v.HEALTH or 150
end
----------------------------------------------------------
--Reforged Tuning
----------------------------------------------------------
--difficulties are numerical, from 1 to 3 or more.
TUNING.BUTTERFLYP_HEALTH = 1
TUNING.BUTTERFLYP_HUNGER = 75
TUNING.BUTTERFLYP_SANITY = 125


return TUNING