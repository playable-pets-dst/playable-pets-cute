-- This information tells other players more about the mod
name = "[MERGED INTO PP] Playable Pets -Cute"
author = "Leonardo Coxington"
version = "1.9"
description = "HEY this mod is now merged into the original PP mod. Don't enable this. \nVersion:"..version
-- This is the URL name of the mod's thread on the forum; the part after the ? and before the first & in the url
forumthread = "/topic/73911-playable-pets/"

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = name.." Dev."
end

-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
all_clients_require_mod = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

priority = -310

forge_compatible = true

server_filter_tags = {

}

local availableMobs = {
	
}

local defaultSkinName = "Something Different" -- Used if a name is not specified in the skins table

-- For Modded Inventory. Please note that this modified table format requires code at the bottom of this file to make it usable for modded inventory.f
menu_assets =
{
	skins = 
	{
		
	},
}

local SETTING = {
	OFF = "Off",
	DISABLE = "Disable",
	ENABLE = "Enable",
	LOCKED = "Locked",
	
	-- Mob Presets
	ALL_MOBS = "AllMobs",
	NO_GIANTS = "NoGiants",
	NO_BOSSES = "NoBosses",
	BOSSES_ONLY = "BossesOnly",
	GIANTS_ONLY = "GiantsOnly",
	CRAFTY_MOBS = "CraftyMobsOnly",
	
	-- Mob Houses
	HOUSE_CRAFT_ONLY = "Enable2",
	HOUSE_ON_SPAWN = "Enable1",
	HOUSE_BOTH = "Enable3",
	
	-- PvP Damage
	PVP_50_PERCENT_DMG = 0.5,
	PVP_100_PERCENT_DMG = 1.0,
	PVP_150_PERCENT_DMG = 1.5,
	PVP_200_PERCENT_DMG = 2.0,
	
	-- Misc
	HUMANOID_SANITY_ONLY = "Disable1",
	MONSTER_CHARCHANGE_ONLY = "Enable1",
}

configuration_options = {
	
}

---------------------------------
-- TABLE POPULATION CODE BELOW --
---------------------------------

-- Automatically populate enable/disable configuration settings for mobs
local settingEnable = {
	{description = "Enabled", data = SETTING.ENABLE},
	{description = "Disabled", data = SETTING.DISABLE},
}

for i = 1, #availableMobs do	
	local configOption = {}
	configOption.name = availableMobs[i]
	configOption.label = availableMobs[i]
	configOption.options = settingEnable
	configOption.default = SETTING.ENABLE
	
	configuration_options[#configuration_options + 1] = configOption
end
